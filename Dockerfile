#
# IGB Dockerfile
# Docker image contains
## Oracle JAVA 1.8
## MAVEN 3.5.0
## Install4j 7.0.8
# https://bitbucket.org/lorainelab/igb-maven-install4j
# Author: Sanket Patil(spatil26)
# Team: IGB (http://www.bioviz.org/igb)
#
FROM maven:3.5.0

RUN \
    #Updating apt package list
    apt-get update && \
    #installing Nano editor
    apt-get install -y nano && \
    #installing Zip tool
    apt-get install -y zip && \
    #Removing openjdk, as openjdk 1.8 does contains all javafx libraries
    apt-get purge -y openjdk-\* && \
    rm docker-java-home && \
    #Downloading oracle jdk 
    wget --no-check-certificate -c --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u191-b12/2787e4a523244c269598db4e85c51e0c/jdk-8u191-linux-x64.tar.gz && \
    #Installing Oracle JDK
    mkdir /usr/lib/jvm && \
    cp jdk-8u191-linux-x64.tar.gz /usr/lib/jvm/ && \
    tar xvzf jdk-8u191-linux-x64.tar.gz -C /usr/lib/jvm/ && \
    ln -sf usr/lib/jvm/jdk1.8.0_191 docker-java-home && \
    update-alternatives --install "/usr/bin/java" "java" "/usr/lib/jvm/jdk1.8.0_191/bin/java" 1 && \
    update-alternatives --install "/usr/bin/javac" "javac" "/usr/lib/jvm/jdk1.8.0_191/bin/javac" 1 && \
    update-alternatives --install "/usr/bin/javaws" "javaws" "/usr/lib/jvm/jdk1.8.0_191/bin/javaws" 1 && \
    #Installing Install4J
    wget https://download-keycdn.ej-technologies.com/install4j/install4j_linux_7_0_8.deb && \
    dpkg -i install4j_linux_7_0_8.deb && \
    #Fetching jre bundles required for install4J maven plugin
    wget https://bitbucket.org/lorainelab/jre-bundles-install4j/downloads/windows-x86-1.8.0_192.tar.gz && \
    wget https://bitbucket.org/lorainelab/jre-bundles-install4j/downloads/windows-amd64-1.8.0_192.tar.gz && \
    wget https://bitbucket.org/lorainelab/jre-bundles-install4j/downloads/macosx-amd64-1.8.0_192_unpacked.tar.gz && \
    wget https://bitbucket.org/lorainelab/jre-bundles-install4j/downloads/linux-amd64-1.8.0_191.tar.gz && \
    #Moving jre bundle to /opt/install4j7/jres
    cp *.tar.gz /opt/install4j7/jres/ && \
    #Cleaing dowloaded stuff
	rm install4j_linux_7_0_8.deb && \
	rm jdk-8u191-linux-x64.tar.gz && \
    rm *.tar.gz
CMD ["bash"]